﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fin_1
{
    class Fleet
    {
        private string Name;
        List<Fleet> ListFleet = new List<Fleet>();

        public void SetName(string name)
        {
            Name = name;            
        }
        public bool CreateShip(int count)
        {
            if (Name == "") { return false; }

            int Limit = count + ListFleet.Count;
            for (int i = ListFleet.Count; i < Limit; i++)
            {
                Ship ship = new Ship();
                string tempname = Name + i;

                ship.SetName(tempname);
            }

            return true;
        }
    }
}
