﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fin_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Ship ship = new Ship();

            ////Used a function
            //ship.SetName("Ship_01");
            ////Using Properties
            //ship._Speed = 100;    

            ////Accessed a Property
            //Console.WriteLine(ship.GetName());
            ////Accessed a Function
            //Console.WriteLine(ship._Speed);

            Fleet fleet = new Fleet();
            Console.WriteLine("Fleet Name: ");
            string inputname = Console.ReadLine();
            fleet.SetName(inputname);

            Console.WriteLine("Spawn number of ships");
            int count = Convert.ToInt32(Console.ReadLine());
            if (fleet.CreateShip(count) == true)
            {
                Console.WriteLine("Ships Spawned!");
            }
            else
            {
                Console.WriteLine("Error!");
            }

        }
    }
}
