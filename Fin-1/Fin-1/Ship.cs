﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fin_1
{
    class Ship
    {
        private string Name;
        private int Speed;

        //Property
        public int _Speed
        {
            get{ return Speed; }
            set{ Speed  = value; }          
        }

        //Function
        public string GetName()
        {
            return Name;
        }
        

        public void SetName(string name)
        {
            Name = name;
        }
    }
}
