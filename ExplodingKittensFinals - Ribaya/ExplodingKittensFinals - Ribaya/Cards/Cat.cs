﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittensFinals___Ribaya
{
    class Cat : Card
    {
        public enum catcards
        {           
            Rainbow,
            Taco,
            Beard,
            Melon,
            HairyPotato
        }
        public Cat(catcards Type)
        {
            card_type = cards.Cat;         
            catcardname = Type;
        }
      
        public catcards catcardname;
    }
}
