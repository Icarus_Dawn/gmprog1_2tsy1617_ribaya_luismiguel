﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittensFinals___Ribaya
{
    class Deck
    {

        public List<Card> Cards;
        Random random = new Random();

        public Deck()
        {
            Cards = new List<Card>();
        }
        public void Shuffle()
        {
            List<Card> Shuffled = new List<Card>();
            List<Card> temp = new List<Card>(Cards);

            while (temp.Count > 0)
            {
                Card card = temp[random.Next(0, temp.Count)];
                Shuffled.Add(card);
                temp.Remove(card);
            }
            Cards = new List<Card>(Shuffled);
        }

        public void CreateCards(int playercount)
        {

            for (int x = 0; x < playercount - 1; x++)
            {
                Cards.Add(new ExplodingKitten());
            }

            for (int x = 0; x < 6 - playercount; x++)
            {
                Cards.Add(new Defuse());
            }

            for (int x = 0; x < 5; x++)
            {
                Cards.Add(new Nope());
            }

            for (int x = 0; x < 4; x++)
            {
                Cards.Add(new ShuffleCard());
                Cards.Add(new SkipCard());
                Cards.Add(new SeeFuture());
                Cards.Add(new Attack());
                Cards.Add(new Favor());
            }

            for (int x = 0; x < 5; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    Cat temp = new Cat((Cat.catcards)x);
                    Cards.Add(temp);
                }
            }

        }

        public Card Draw()
        {
            Card temp = Cards.First();
            Cards.Remove(temp);
            return temp;
        }
    }
}
