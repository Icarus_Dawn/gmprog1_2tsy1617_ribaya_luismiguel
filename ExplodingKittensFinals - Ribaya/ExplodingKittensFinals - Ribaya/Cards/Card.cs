﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittensFinals___Ribaya
{
    class Card
    {
        public bool isExplodingKitten = false;
        public bool isDefuseCard = false;
        public cards card_type;
        public enum cards
        {
            type,
            ExplodingKitten,
            ShuffleCard,
            SkipCard,
            Defuse,
            Nope,
            SeeFuture,
            Favor,
            Cat,
            Attack
        };

        public virtual void UseAbility()
        {

        }      
    }
}
