﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittensFinals___Ribaya
{
    class GameManager
    {
        Deck deck = new Deck();
        List<Player> players = new List<Player>();
        List<Card> Discard = new List<Card>();
        int playerturn = 1;

        public void AddPlayers()
        {
            for (int x = 0; x < 4; x++)
            {
                players.Add(new Player());
            }
        }

        public void MakeCards()
        {
            deck.CreateCards(players.Count);
            deck.Shuffle();
        }

        public void PlayerDraw()
        {
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < players.Count; y++)
                {
                    players[y].CardList(deck.Draw());
                }
            }
        }

        public void PrintPlayerCards()
        {
            for (int x = 0; x < players.Count; x++)
            {
                Console.WriteLine("Player: " + (x+1));
                players[x].Print();
                Console.ReadLine();
            }
        }


        public GameManager()
        {
            AddPlayers();
            MakeCards();
            PlayerDraw();
            PrintPlayerCards();
            Console.Clear();

            while (players.Count > 1)
            {               
                int index = 0;
                while (index == 0)
                {
                    Console.WriteLine("Player: " + playerturn);
                    Console.WriteLine("1. Draw(End turn)");
                    Console.WriteLine("2. Play Card Ability");
                    Console.WriteLine("3. Take Card from Enemy");
                    Console.Write("-> ");                    

                    if (int.TryParse(Console.ReadLine(), out index) == true)
                    {
                        switch (index)
                        {
                            case 1:
                                Console.Clear();
                                break;
                            case 2:
                                Console.Clear();
                                break;
                            case 3:
                                Console.Clear();
                                break;
                            default:
                                Console.WriteLine("Error! Pls choose a valid option");
                                Console.Clear();
                                break;
                        }
                    }                
                }
                if (playerturn >= players.Count - 1)
                {
                    playerturn = 1;
                }
                else
                {
                    playerturn++;
                }
                Console.Clear();
            }
            Console.Read();
            Console.Clear();
        }
    }
}
