﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittensFinals___Ribaya
{
    class Player
    {
        public List<Card> PlayerHand;
      
        public Player()
        {
            PlayerHand = new List<Card>();
        }
        public void CardList(Card card)
        {
            PlayerHand.Add(card);
        }
        public void Print()
        {
            for (int x = 0; x < PlayerHand.Count; x++)
            {
                if (PlayerHand[x].GetType() == typeof(Cat))
                {
                    Console.WriteLine((PlayerHand[x] as Cat).catcardname.ToString());
                }
                else
                {
                    Console.WriteLine(PlayerHand[x].card_type.ToString());
                }
            }
        }

        //public Card UseAbility()
        //{
        //Using Abilities
        //}

        //public Card DrawHand()
        //{
        //    Card temp = PlayerHand.First();
        //    PlayerHand.Remove(temp);
        //    return temp;
        //}
    }
}
