﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizMaid___Ribaya
{
    class Suit
    {
        Card card = new Card();
        List<Card> listcards = new List<Card>();

        public enum suit { type, Club, Spade, Diamond, Heart };
        protected suit suittype = suit.type;
        Random random = new Random();

        public string GetSuitType()
        {
            return suittype.ToString();
        }

        public string GetCardname()
        {
            return GetCardname().ToString();
        }

        public void PrintCards()
        {
            Console.Write(suittype + ": ");
            foreach (Card card in listcards)
            {
                Console.Write(card.GetCardName + " ");
            }
            Console.Write("\n");
        }

        protected void Intializecards()
        {
            Card Ace = new Card();
            Ace.GetCardName = "Ace";
            listcards.Add(Ace);

            for (int x = 0; x < 3; x++)
            {
                Card Royals = new Card();
                switch (x)
                {
                    case 0:
                        Royals.GetCardName = "King";
                        break;
                    case 1:
                        Royals.GetCardName = "Queen";
                        break;
                    case 2:
                        Royals.GetCardName = "Jack";
                        break;
                }
                listcards.Add(Royals);
            }

            for (int y = 1; y < 10; y++)
            {
                Card number = new Card();
                int tempname = y + 1;
                number.GetCardName = tempname.ToString();
                listcards.Add(number);
            }
            PrintCards();
        }

    }
}
