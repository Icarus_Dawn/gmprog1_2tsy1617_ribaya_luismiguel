﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class PElf : Characters
    {
        public PElf()
        {
            _CharClass = CharClass.Elf;
            _CharType = CharType.playable;
            Attack = 30;
            Health = 15;
        }

        public override void Greeting()
        {
            base.Greeting();
            Dialogue("At your Service Mistress!");
        }
    }
}
