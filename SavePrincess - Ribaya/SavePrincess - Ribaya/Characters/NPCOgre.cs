﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class NPCOgre : Characters
    {
        public NPCOgre()
        {
            _CharClass = CharClass.Ogre;
            _CharType = CharType.npc;
            Attack = 20;
            Health = 20;
        }

        public override void Win()
        {
            base.Win();
            Dialogue("PUNY HUMAN! THIS IS WHAT DEATH LOOKS LIKE UP CLOSE!\n\n");
        }

        public override void Taunt()
        {
            base.Taunt();
            Dialogue("GONNA FEAST ON YOUR FLESH!!!");
        }
    }
}
