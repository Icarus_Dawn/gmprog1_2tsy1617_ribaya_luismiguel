﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Characters
    {
        public enum CharType
        {
            playable,
            npc
        }

        public enum CharClass
        {
            Wizard,
            Elf,
            Ogre,
            Dragon,
            Prince
        }

        protected CharClass _CharClass;
        protected CharType _CharType;

        protected int Health = 0;
        protected int Attack = 0;
        protected bool checkVisible = true;

        public string GetCharClass()
        {
            return _CharClass.ToString();
        }

        public int GetHealth()
        {
            return Health;
        }

        public int GetAttack()
        {
            return Attack;
        }

        public bool GetcheckVisible()
        {
            return checkVisible;
        }

        public void Damage(int Damage)
        {
            Health -= Damage;
        }

        public void RecieveItem(bool visible, int health, int attack)
        {
            Attack += attack;
            Health += health;
            checkVisible = visible;
        }

        protected void Dialogue(string _Dialogue)
        {
            Console.WriteLine(_CharClass.ToString() + " -> " + _Dialogue);
        }

        public virtual void Taunt()
        {

        }

        public virtual void Win()
        {

        }

        public virtual void Greeting()
        {

        }

        public void DisplayStats()
        {
            Console.WriteLine("-> " + _CharClass.ToString()
                                + " Stats = "
                                + "Health: " + Health
                                + ", Attack: " + Attack);
        }


    }
}
