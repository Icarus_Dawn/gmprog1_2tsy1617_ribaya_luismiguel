﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class NPCDragon : Characters
    {
        public NPCDragon()
        {
            _CharClass = CharClass.Dragon;
            _CharType = CharType.npc;
            Attack = 30;
            Health = 75;
        }

        public override void Win()
        {
            base.Win();
            Dialogue("BURN YOU INFERIOR BEING.\n\n");         
        }

        public override void Taunt()
        {
            base.Taunt();
            Dialogue("My teeth are swords. My claws are spears. My wings are a HURRICANE!");
        }
    }
}
