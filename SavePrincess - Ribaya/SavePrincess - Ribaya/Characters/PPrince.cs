﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class PPrince : Characters
    {
        public PPrince()
        {
            _CharClass = CharClass.Prince;
            _CharType = CharType.playable;
            Attack = 20;
            Health = 30;
        }

        public override void Greeting()
        {
            base.Greeting();
            Dialogue("Wait for me my princess!");
        }
    }
}
