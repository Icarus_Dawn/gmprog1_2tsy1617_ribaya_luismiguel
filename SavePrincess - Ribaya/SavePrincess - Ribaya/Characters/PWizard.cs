﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class PWizard : Characters
    {
        public PWizard()
        {
            _CharClass = CharClass.Wizard;
            _CharType = CharType.playable;
            Attack = 15;
            Health = 20;
        }
        public override void Greeting()
        {
            base.Greeting();
            Dialogue("Magic comes with a price Mi Lady!");
        }

    }
}
