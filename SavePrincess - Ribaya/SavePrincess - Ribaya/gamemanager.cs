﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class gamemanager
    {
        Random random = new Random();

        List<Characters> PlayableList = new List<Characters>();
        List<Characters> NPCList = new List<Characters>();
        List<Items> ItemsList = new List<Items>();
        List<Items> FallingObjectList = new List<Items>();
        bool success;
        Characters Player;
        Characters Enemy;
        Items Loot;
        Items FallingObject;

        private void DialoguePrincess()
        {
            Console.WriteLine("Princess: Help Me!");
            Console.WriteLine("-> a princess yells for help in a castle not far");
        }

        private void PlayerWin()
        {
            Console.WriteLine("-> The " + Enemy.GetCharClass() + " wimpers in pain and dies");
        }

        private void PlayerLose()
        {
            Console.WriteLine("-> The " + Enemy.GetCharClass() 
                                + " laughs while you die a slow and agonizing death");
            Enemy.Win();
        }

        private void Initializer()
        {
            PPrince prince = new PPrince();
            PlayableList.Add(prince);
            PElf elf = new PElf();
            PlayableList.Add(elf);
            PWizard wizard = new PWizard();
            PlayableList.Add(wizard);

            NPCDragon dragon = new NPCDragon();
            NPCList.Add(dragon);
            NPCOgre ogre = new NPCOgre();
            NPCList.Add(ogre);

            Elixir elixir = new Elixir();
            ItemsList.Add(elixir);
            Cloak cloak = new Cloak();
            ItemsList.Add(cloak);
            Mana mana = new Mana();
            ItemsList.Add(mana);

            Rat rat = new Rat();
            FallingObjectList.Add(rat);
            Plate plate = new Plate();
            FallingObjectList.Add(plate);
            Rock rock = new Rock();
            FallingObjectList.Add(rock);
        }

        private Characters RandomCharacter()
        {
            int index = random.Next(0, PlayableList.Count());
            return PlayableList[index];
        }

        private Characters RandomEnemy()
        {
            int index = random.Next(0, NPCList.Count());
            return NPCList[index];
        }

        private Items CreateFallingObject()
        {
            int index = random.Next(0, FallingObjectList.Count());
            return FallingObjectList[index];
        }

        private Items CreateLoot()
        {
            int index = random.Next(0, ItemsList.Count());
            return ItemsList[index];
        }

        private bool CheckPlayerWin()
        {
            Player.Damage(Enemy.GetAttack());
            Enemy.Damage(Player.GetAttack());
            Enemy.DisplayStats();
            Player.DisplayStats();

            if (Player.GetHealth() <= 0)
            {
                return false;
            }
            else if(Player.GetHealth() < Enemy.GetHealth())
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private bool CheckPlayerFallingObject()
        {
            if (Player.GetHealth() <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public gamemanager()
        {
            Initializer();

            DialoguePrincess();
            Player = RandomCharacter();
            Player.Greeting();
            Player.DisplayStats();
            Console.WriteLine("-> you see something interesting...");
            Loot = CreateLoot();
            Loot.DisplayItem();
            Loot.ConsumeItem(Player);            
            Console.WriteLine("-> You encounter an enemy!");
            if(Player.GetcheckVisible() == true)
            {
                Enemy = RandomEnemy();
                Enemy.Taunt();
                Enemy.DisplayStats();
                Player.DisplayStats();
                Console.WriteLine("-> you engage and deal " + Player.GetAttack() + " damage to the enemy!");
                success = CheckPlayerWin();
                if (success == true)
                {
                    PlayerWin();
                }
                else
                {
                    PlayerLose();
                }
            }

            else if (Player.GetcheckVisible() == false)
            {
                success = true;
                Enemy = RandomEnemy();
                Console.WriteLine("-> you see:" + " " + Enemy.GetCharClass());
                Console.WriteLine("-> you sneak past with the Invisible Cloak");                   
            }

            if (success == true)
            {
                FallingObject = CreateFallingObject();
                FallingObject.ConsumeItem(Player);
                Console.WriteLine("-> You suck!, A " + FallingObject.ItemsName.ToString()
                                    + " fell on your head");               
                Player.DisplayStats();
                success = CheckPlayerFallingObject();
                if (success == true)
                {
                    Console.WriteLine("-> the only that hurts is your pride, you lived happily ever after!");
                    Console.WriteLine("-> you leave the castle and get the princess to safety!\n\n");
                }
                else
                {
                    Console.WriteLine("-> you dieded!\n\n");
                }
            }                     
        }
    }
}
