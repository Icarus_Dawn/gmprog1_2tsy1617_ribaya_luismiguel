﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Items
    {
        public enum ItemsType
        {
            Cloak,
            Mana,
            Elixir,
            Rock,
            Rat,
            Plate
        }
        public ItemsType ItemsName;

        public virtual void DisplayItem()
        {
            Console.Write("-> You have a: "
                           + ItemsName.ToString()
                           + ", ");
        }

        public virtual void ConsumeItem(Characters player)
        {

        }
    }
}
