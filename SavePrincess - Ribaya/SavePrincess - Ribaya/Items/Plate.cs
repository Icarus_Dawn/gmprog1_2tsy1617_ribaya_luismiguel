﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Plate : Items
    {
        public Plate()
        {
            ItemsName = ItemsType.Plate;
        }

        public override void ConsumeItem(Characters player)
        {
            base.ConsumeItem(player);
            player.RecieveItem(true, -10, 0);
        }
    }
}
