﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Rat : Items
    {
        public Rat()
        {
            ItemsName = ItemsType.Rat;
        }

        public override void ConsumeItem(Characters player)
        {
            base.ConsumeItem(player);
            player.RecieveItem(true, -30, 0);
        }
    }
}
