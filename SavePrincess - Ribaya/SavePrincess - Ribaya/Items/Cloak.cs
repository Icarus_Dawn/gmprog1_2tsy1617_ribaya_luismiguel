﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Cloak : Items
    {
        public Cloak()
        {
            ItemsName = ItemsType.Cloak;
        }

        public override void DisplayItem()
        {
            base.DisplayItem();
            Console.WriteLine("Gives Invisibility");
        }

        public override void ConsumeItem(Characters player)
        {
            base.ConsumeItem(player);
            player.RecieveItem(false, 0, 0);
        }
    }
}
