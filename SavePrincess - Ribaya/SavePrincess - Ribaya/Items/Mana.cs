﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Mana : Items
    {
        public Mana()
        {
            ItemsName = ItemsType.Mana;
        }

        public override void DisplayItem()
        {
            base.DisplayItem();
            Console.WriteLine("Increases Player Damage x3");
        }

        public override void ConsumeItem(Characters player)
        {
            base.ConsumeItem(player);
            player.RecieveItem(true, 0, player.GetAttack() * 3);
        }
    }
}
