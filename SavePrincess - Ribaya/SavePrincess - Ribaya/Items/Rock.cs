﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Rock : Items
    {
        public Rock()
        {
            ItemsName = ItemsType.Rock;
        }

        public override void ConsumeItem(Characters player)
        {
            base.ConsumeItem(player);
            player.RecieveItem(true, -20, 0);
        }
    }
}
