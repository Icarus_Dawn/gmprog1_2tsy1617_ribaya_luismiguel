﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavePrincess___Ribaya
{
    class Elixir : Items
    {
        public Elixir()
        {
            ItemsName = ItemsType.Elixir;
        }

        public override void DisplayItem()
        {
            base.DisplayItem();
            Console.WriteLine("Gives 50 Health");
        }

        public override void ConsumeItem(Characters player)
        {
            base.ConsumeItem(player);
            player.RecieveItem(true, 50, 0);
        }
    }
}
