﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1___Ribaya
{
    class Program
    {
        static void Main(string[] args)
        {
            Person Persons = new Person();
            string input;

            Console.Write("What is your First Name?: \n->");
            Persons.First_Name = Console.ReadLine();
            Console.Clear();

            Console.Write("What is your Last Name?: \n->");
            Persons.Last_Name = Console.ReadLine();
            Console.Clear();

            do
            {
                Console.Write("What is your Age?: \n->");
                Persons.Age = Convert.ToInt32(Console.ReadLine());
                Console.Clear();

                if (Persons.Age >= 18 && Persons.Age < 70)
                {
                    Console.WriteLine("Age Appropriate!, Access Granted");

                    Console.Write("What is your Occupation?: \n->");
                    Persons.Job = Console.ReadLine();
                    Console.Clear();
                }
                else if (Persons.Age < 18 && Persons.Age > 0)
                {
                    Console.WriteLine("You are not old enough to input an Occupation!, Access Denied!");
                    
                }
                else if (Persons.Age >= 70 || Persons.Age <= 0)
                {
                    Console.WriteLine("Age out of Appropriate Range! Access Denied!");
                }
            }
            while (Persons.Age <= 0 || Persons.Age >= 70);

            Console.Write("What is your Birth Month?: \n->");
            Persons.Birth_Month = Console.ReadLine();
            Console.Clear();

            Console.Write("What is your Birth Day?: \n->");
            Persons.Birth_Day = Console.ReadLine();
            Console.Clear();

            Console.Write("What is your Birth Year?: \n->");
            Persons.Birth_Year = Console.ReadLine();
            Console.Clear();

            Console.Write("What is your Weight in Pounds?: \n->");
            Persons.Weight = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            Console.Write("Do you want to Save this Information (Y/N)?: \n->");
            input = Console.ReadLine();
            switch (input)
            {
                case "Y":
                case "y":
                case "Yes":
                case "YES":
                case "yes":
                    Persons.saveinfo = true;
                    break;
                case "N":
                case "n":
                case "no":
                case "NO":
                case "No":
                    Persons.saveinfo = false;
                    break;
            }
            Console.Clear();

            if (Persons.saveinfo == true)
            {
                Console.WriteLine("Thank you!\n" + "Saved Information: \n" + "\nName: " + Persons.Last_Name + ", "
                                     + Persons.First_Name + "\nBirthday: " + Persons.Birth_Month + ", "
                                     + Persons.Birth_Day + ", " + Persons.Birth_Year + "\nAge: " + Persons.Age
                                     + "\nWeight: " + Persons.Weight + "Lbs" + "\nOccupation: " + Persons.Job);
            }
            else //if (Persons.saveinfo == false)
            {
                Console.Write("Data has not been saved! Thank you!");
                Console.Read();
            }
        }
    }
}