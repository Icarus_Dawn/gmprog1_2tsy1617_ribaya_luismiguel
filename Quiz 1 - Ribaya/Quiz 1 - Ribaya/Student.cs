﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz_1___Ribaya
{
   public class Student
    {
        public string FirstName;
        public string LastName;
        public int IDNum;
        public string NewEnroll;

        public Student(string firstname, int idnum, string lastname, string newenroll)
        {
            FirstName = firstname;
            IDNum = idnum;
            LastName = lastname;
            NewEnroll = newenroll;
        }
    }
}
