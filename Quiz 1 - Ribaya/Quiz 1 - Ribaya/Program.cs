﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz_1___Ribaya
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instances w/Default Values
            Student student1 = new Student("John", 0, "Doe", "default");
            BlockSec blocksec1 = new BlockSec("Default", 0, 0, 0);

            //Block
            Console.WriteLine("Course Code:");
            Console.Write("-> ");
            blocksec1.CourseCode = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("School Year:");
            Console.Write("-> ");
            blocksec1.SY = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Term:");
            Console.Write("-> ");
            blocksec1.Term = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Class Limit:");
            Console.Write("-> ");
            blocksec1.Limit = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            ////Enrollee Question
            //Console.WriteLine("Are you a New Enrollee?");
            //Console.Write("-> ");
            //student1.NewEnroll = Console.ReadLine();

            //Strings
            string[] NewEnroll = new string[blocksec1.Limit];
            string[] OldEnroll = new string[blocksec1.Limit];


            //New Enrollees
            for (int x = 0; x < blocksec1.Limit; x++)
            {
                if (NewEnroll[blocksec1.Limit - 1] == null)
                {
                    //Student
                    Console.WriteLine("Enter First Name:");
                    Console.Write("-> ");
                    NewEnroll[x] = Console.ReadLine();
                    Console.Clear();

                    Console.WriteLine("Enter Last Name:");
                    Console.Write("-> ");
                    student1.LastName = Console.ReadLine();
                    Console.Clear();

                    Console.WriteLine("Enter ID Number:");
                    Console.Write("-> ");
                    student1.IDNum = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("Block is Full");
                    foreach (string name in NewEnroll)
                    {
                        Console.WriteLine("Block Info");
                        Console.WriteLine("Course: {0}", blocksec1.CourseCode);
                        Console.WriteLine("Term: {0}", blocksec1.Term);
                        Console.WriteLine("School Year: {0}", blocksec1.SY);
                        Console.WriteLine("Name: {0} {1}, ID#: {2}", NewEnroll, student1.LastName, student1.IDNum);
                        Console.WriteLine("Do you Want to Make a new Block?");
                    }
                }

            }

        }

    }
}
