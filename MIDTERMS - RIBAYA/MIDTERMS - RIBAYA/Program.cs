﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIDTERMS___RIBAYA
{
    class Program
    {
        static void Main(string[] args)
        {
            FleetManager fleetmanager = new FleetManager();
            Fleet fleet = new Fleet();

            fleetmanager.Introduction();
            do
            {                
                fleetmanager.MainMenu();

                int input = Convert.ToInt32(Console.ReadLine());

                switch (input)
                {
                    case 1:
                        //Make Fleet

                        Console.Clear();

                        Console.Write("Fleet Name: ");
                        fleet.FName = Console.ReadLine();

                        Console.Clear();

                        Console.Write("Fleet's Speed: ");
                        fleet.FSpeed = Convert.ToInt32(Console.ReadLine());

                        Console.Clear();

                        Console.Write("Number of Ships: ");
                        int ShipCount = Convert.ToInt32(Console.ReadLine());
                        fleet.addships(ShipCount);

                        Console.Clear();

                        Console.WriteLine("Input Sccessful!\n");
                        fleetmanager.AddFleet(fleet);
                        break;

                    case 2:
                        //Print Existing Fleets to Console

                        fleetmanager.PrintFleet();                     

                        break;

                    case 3:
                        //Add Starships to Fleet

                        fleetmanager.PrintFleet();
                        Console.Write("\nPick a Fleet: ");
                        string tmpFname = Console.ReadLine();
                        Fleet AddShip = fleetmanager.FleetSearch(tmpFname);
                        if (AddShip == null)
                        {
                            Console.WriteLine("Fleet Doesn't Exist!");
                        }
                        else
                        {
                            Console.Write("Number of Ships to Add: ");
                            int AddShipCount = Convert.ToInt32(Console.ReadLine());
                            AddShip.addships(AddShipCount);
                            Console.Clear();
                            Console.WriteLine("Ships Successfully Added!\n");
                        }

                        break;

                    case 4:
                        //Remove Starships from Fleet

                        fleetmanager.PrintFleet();
                        Console.Write("\nPick a Fleet: ");
                        tmpFname = Console.ReadLine();
                        Fleet Remove = fleetmanager.FleetSearch(tmpFname);
                        if (Remove == null)
                        {
                            Console.WriteLine("Fleet Doesn't Exist!");
                        }
                        else
                        {
                            Console.Write("Number of Ships to Delete: ");
                            int DeleteShipCount = Convert.ToInt32(Console.ReadLine());
                            Remove.removeship(DeleteShipCount);
                            Console.Clear();
                            Console.WriteLine("Ships Successfully Deleted!");
                            Console.WriteLine("Existing Fleets: \n");
                            Remove.PrintNames();
                        }

                        break;

                    case 5:
                        //Display Fleet Information

                        fleetmanager.PrintFleet();
                        Console.Write("\nPick a Fleet: ");
                        tmpFname = Console.ReadLine();
                        Fleet Info = fleetmanager.FleetSearch(tmpFname);
                        Console.Clear();
                        if (Info == null)
                        {
                            Console.WriteLine("Fleet Doesn't Exist!\n");
                        }
                        else
                        {                         
                            Console.WriteLine("Currently Existing Ships in chosen Fleet:");
                            Console.WriteLine("Speed: Warp {0}\n", fleet.FSpeed);                       
                            Info.PrintNames();
                        }

                            break;

                    case 6:
                        //Delete a Fleet

                        fleetmanager.PrintFleet();
                        Console.Write("\nPick a Fleet: ");
                        tmpFname = Console.ReadLine();
                        Fleet deletefleet = fleetmanager.FleetSearch(tmpFname);
                        fleetmanager.DeleteFleet(deletefleet);

                        Console.Clear();

                        Console.WriteLine("Fleet Successfully Deleted!\n");

                        break;

                    case 7:
                        //Exit Program

                        fleetmanager.MenuStatus = false;
                        break;
                }
            } while (fleetmanager.MenuStatus == true);
        }
    }
}
