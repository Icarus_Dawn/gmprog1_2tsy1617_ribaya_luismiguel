﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIDTERMS___RIBAYA
{
    class FleetManager
    {
        public List<Fleet> ListFleet = new List<Fleet>();
        public bool MenuStatus = true;

        public void Introduction()
        {
            Console.WriteLine("WELCOME TO THE FLEET CREATOR v.5!");
            Console.WriteLine("What would you like to do?\n");
        }

        public void MainMenu()
        {           
            Console.WriteLine("1. Create Fleet");
            Console.WriteLine("2. Display Fleets");
            Console.WriteLine("3. Add Starships to Fleet");
            Console.WriteLine("4. Remove Starships");
            Console.WriteLine("5. Display Fleet Info");
            Console.WriteLine("6. Delete a Fleet");
            Console.WriteLine("7. Exit Program");
            Console.Write("-> ");            
        }

        public void AddFleet(Fleet fleet)
        {
            ListFleet.Add(fleet);
        }

        public void DeleteFleet(Fleet fleet)
        {
            ListFleet.Remove(fleet);
        }

        public void PrintFleet()
        {
            Console.Clear();
            Console.WriteLine("Current Existing Fleets: \n");
            foreach (Fleet fleet in ListFleet)
            {                
                Console.WriteLine("{0}\n", fleet.FName);
            }
        }

        public Fleet FleetSearch(string name)
        {
            foreach (Fleet fleet in ListFleet)
            {
                if (fleet.FName == name)
                {
                    return fleet;
                }                
            }
            return null;
        }
    }
}
