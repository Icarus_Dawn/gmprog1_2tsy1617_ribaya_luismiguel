﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIDTERMS___RIBAYA
{
    class Fleet
    {
        public int FSpeed;
        public string FName;

        public List<Starship> ListStarships = new List<Starship>();
        

        public void addships(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Starship starship = new Starship();
                starship.Number = i;                
                ListStarships.Add(starship);
                starship.Name = FName;
            }
        }

        public void removeship(int count)
        {           
            for (int x = 0; x < count; x++)
            {
                Starship starship = new Starship();
                starship = ListStarships[x];
                ListStarships.Remove(starship);
            }        
        }      

        public void PrintNames()
        {          
            foreach (Starship starship in ListStarships)
            {    
                           
                Console.WriteLine("{0} X-{1}\n", starship.Name, starship.Number);
            }
        }
    }
}
