﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciseDeck___Ribaya
{
    class Program
    {
        static void Main(string[] args)
        {
            CardManager cardmanager = new CardManager();
            
            for (;;)
            {              
                Console.Write("Draw! \n");
                cardmanager.DrawCards(0);
                Console.ReadLine();              
                if (cardmanager.GetCash() <= 0)
                {
                    break;
                }
            }
            Console.WriteLine("Game Over!");
            Console.ReadLine();
        }
    }
}
