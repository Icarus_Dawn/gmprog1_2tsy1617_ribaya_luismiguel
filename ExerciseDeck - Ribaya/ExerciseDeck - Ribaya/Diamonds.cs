﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciseDeck___Ribaya
{
    class Diamonds : Suit
    {
        public Diamonds()
        {
            suit_type = suit.Diamonds;
            multiplier = 3;

            InitializeCards();
        }
    }
}
