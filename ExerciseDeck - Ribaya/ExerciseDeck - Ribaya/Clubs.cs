﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciseDeck___Ribaya
{
    class Clubs : Suit
    {
        public Clubs()
        {
            suit_type = suit.Clubs;
            multiplier = 1;
          
            InitializeCards();
        }
    }
}
