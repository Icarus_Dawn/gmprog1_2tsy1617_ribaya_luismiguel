﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciseDeck___Ribaya
{
    class Suit
    {
        Card card = new Card();
        public enum suit {type, Clubs, Spades, Diamonds, Hearts};
        protected suit suit_type = suit.type;
        protected int multiplier;
        List<Card> Suitcards = new List<Card>();
        Random random = new Random();

        public Card pick()
        {
            int index = random.Next(0, Suitcards.Count());
            return Suitcards[index];
        }

        public int GetCValue(Card card)
        {
            int value = multiplier * card.cost;
            return value;
        }

        public string GetSType()
        {
            return suit_type.ToString();
        }

        private void Print()
        {            
            Console.Write(suit_type + ": ");
            foreach (Card card in Suitcards)
            {
                Console.Write(card.CName + " ");
            }
            Console.Write("\n");
        }

        protected void InitializeCards()
        {
            Card Ace = new Card();
            Ace.cost = 10;
            Ace.CName = "Ace";
            Suitcards.Add(Ace);

            for (int x = 0; x < 3; x++)
            {
                Card King = new Card();
                King.cost = 5;
                switch (x)
                {
                    case 0:
                        King.CName = "King";
                        break;
                    case 1:
                        King.CName = "Queen";
                        break;
                    case 2:
                        King.CName = "Jack";
                        break;
                }
                Suitcards.Add(King);
            }

            for (int y = 1; y < 10; y++)
            {
                Card number = new Card();
                number.cost = 2;
                int tempname = y + 1;
                number.CName = tempname.ToString();
                Suitcards.Add(number);
            }
            Print();
        }
    }
}
