﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciseDeck___Ribaya
{
    class Spades : Suit
    {
        public Spades()
        {
            suit_type = suit.Spades;
            multiplier = 8;

            Console.WriteLine("Welcome to an Exercise on Inheritance!\n");
            InitializeCards();
        }
    }
}
