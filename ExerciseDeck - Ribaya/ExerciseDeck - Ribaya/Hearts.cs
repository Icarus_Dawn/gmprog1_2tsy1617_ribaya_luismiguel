﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciseDeck___Ribaya
{
    class Hearts : Suit
    {
        public Hearts()
        {
            suit_type = suit.Hearts;
            multiplier = 5;

            InitializeCards();
        }
    }
}
