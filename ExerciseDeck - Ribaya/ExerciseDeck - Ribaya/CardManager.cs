﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciseDeck___Ribaya
{
    class CardManager
    {      
        Spades spades = new Spades();
        Hearts hearts = new Hearts();
        Diamonds diamonds = new Diamonds();
        Clubs clubs = new Clubs();     
        List<Suit> Cards = new List<Suit>();

        protected int Cash = 100;
        Random random = new Random();

        public CardManager()
        {
            Cards.Add(spades);
            Cards.Add(diamonds);
            Cards.Add(clubs);
            Cards.Add(hearts);            
            Console.WriteLine("Cash Amount: {0}\n", Cash);
        }

        public int GetCash()
        {
            return Cash;
        }

        public void DrawCards(int x)
        {
            int cardsindex = random.Next(0, Cards.Count());
            Suit suit_draw = Cards[cardsindex];
            Card carddraw = suit_draw.pick();

            int value = suit_draw.GetCValue(carddraw);
            switch (x)
            {
                case 0:
                    Cash -= value;
                    Console.WriteLine("Cards: {0}, {1}; Cost: {2}", suit_draw.GetSType(), carddraw.CName, value);
                    break;
                case 1:
                    Cash += value;
                    Console.WriteLine("Cards: {0}, {1}; Cost: {2}", suit_draw.GetSType(), carddraw.CName, value);
                    break;
            }
            Console.WriteLine("Cash Left: {0}\n(Press Enter!)", Cash);
        }
    }
}
