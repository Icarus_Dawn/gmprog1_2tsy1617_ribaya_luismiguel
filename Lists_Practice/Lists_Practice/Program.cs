﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists_Practice
{
    class Program
    {
        static void Main(string[] args)
        {
            //Car McQueen = new Car();

            //McQueen.type = "Race Car";
            //McQueen.speed = 100;

            //Car Mater = new Car();

            //Mater.type = "Tow truck";
            //Mater.speed = 50;

            List<Car> ListCars = new List<Car>();

            //Cloning 10 Race Cars and 5 Tow Trucks
            for (int i = 0; i < 10; i++)
            {
                Car McQueen = new Car();

                McQueen.type = "Race Car";
                McQueen.speed = 100;
                McQueen.CarNumber = i;

                ListCars.Add(McQueen);
            }

            for (int j = 0; j < 5; j++)
            {
                Car Mater = new Car();

                Mater.type = "Tow truck";
                Mater.speed = 50;

                Mater.CarNumber = j;

                ListCars.Add(Mater);         
            }

            for (int y = 0; y < 5; y++)
            {
                Console.WriteLine("List of Cars: ");
                foreach (Car car in ListCars)
                {
                    Console.WriteLine(car.type + car.CarNumber);
                }

                Console.WriteLine("\nRemove Car from List");

                string type;
                int number;

                Console.Write("Car Type: ");
                type = Console.ReadLine();
                Console.Write("Car Number: ");
                number = Convert.ToInt32(Console.ReadLine());

                Car sample = new Car();

                for (int x = 0; x < ListCars.Count(); x++)
                {
                    if (ListCars[x].type != type)
                    {
                        continue;
                    }
                    if (ListCars[x].CarNumber != number)
                    {
                        continue;
                    }

                    Console.WriteLine(ListCars[x].type + ListCars[x].CarNumber + " removed!\n");
                    sample = ListCars[x];
                    ListCars.Remove(ListCars[x]);
                }

                if (ListCars.Contains(sample))
                {
                    Console.WriteLine("You did not successfully remove car!\n");
                }
                else
                {
                    Console.WriteLine("Sequence Successful!\n");
                }
            }
            Console.Read();
        }
    }
}
